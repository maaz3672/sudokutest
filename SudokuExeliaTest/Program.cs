﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuExeliaTest
{
    class SudokuPuzzleValidator
    {
        static void Main(string[] args)
        {
            int[][] goodSudoku1 = {
                new int[] {7,8,4,  1,5,9,  3,2,6},
                new int[] {5,3,9,  6,7,2,  8,4,1},
                new int[] {6,1,2,  4,3,8,  7,5,9},

                new int[] {9,2,8,  7,1,5,  4,6,3},
                new int[] {3,5,7,  8,4,6,  1,9,2},
                new int[] {4,6,1,  9,2,3,  5,8,7},

                new int[] {8,7,6,  3,9,4,  2,1,5},
                new int[] {2,4,3,  5,6,1,  9,7,8},
                new int[] {1,9,5,  2,8,7,  6,3,4}
            };

         
            if(ValidateSudoku(goodSudoku1))
            {
                Console.WriteLine("This is supposed to validate! It's a good sudoku!");
            }
            else
                Console.WriteLine("This is supposed to validate! It's a Bad sudoku!");

          //  Debug.Assert(ValidateSudoku(goodSudoku1), "This is supposed to validate! It's a good sudoku!");
        

            Console.ReadLine();
        }

        #region SudokuValidationMethod
        
        static bool ValidateSudoku(int[][] puzzle)
        {
            for (int row = 0; row < 9; row++)
            {
                for (int column = 0; column < 9; column++)
                {
                    if (DoesRowContainValue( row, column, puzzle))
                    {
                       return false;
                    }
                    if (DoesColumnContainValue(row, column, puzzle))
                    {
                        return false; 
                    }
                    if (DoesSquareContainValue(row, column, puzzle))
                    {
                        return false;
                    }
                }
              
            }
            return true;
        }

        #endregion



        #region Validate Functions

        // For Row Validation
        static bool DoesRowContainValue( int row, int column, int[][] puzzle)
        {

            for (int ColumnIndex =column+1; ColumnIndex < 9; ColumnIndex++)
            {

                if (puzzle[row][ColumnIndex] == puzzle[row][column]) 
                {

                    return true;

                }

            }

            return false;

        }

        // For Column Validation
        static bool DoesColumnContainValue(int row, int column,int[][] puzzle)
        {

            for (int rowindex = row+1; rowindex < 9; rowindex++)
            {

                if (puzzle[rowindex][column] == puzzle[row][column])
                {

                    return true;

                }

            }

            return false;

        }

        // For Square Validation
        static bool DoesSquareContainValue( int row, int column, int[][] puzzle)
        {

            //identify square

            int rowStart = ((row - 1) / 3) + 1; 

            int columnStart = ((column - 1) / 3) + 1; 



            int rowIndexEnd = rowStart * 3;

            if (rowIndexEnd == 0)
            {
                rowIndexEnd = 3;
            }

            int columnIndexEnd = columnStart * 3;

            if (columnIndexEnd == 0)
            {
                columnIndexEnd = 3;
            }

            for (int rowIndex = row; rowIndex < rowIndexEnd; rowIndex++)
            {

                for (int columnIndex = column+1; columnIndex < columnIndexEnd; columnIndex++)
                {

                    if ((puzzle[rowIndex][columnIndex] == puzzle[row][column])) // & (columnIndex != column) & (rowIndex != row))
                    {

                        return true;

                    }

                }

            }

            return false;

        }

        #endregion

    }
}
